module s3-quota-usage-exporter

go 1.22.3

require (
	github.com/imacks/aws-sigv4 v0.1.1 // indirect
	github.com/inhies/go-bytesize v0.0.0-20220417184213-4913239db9cf // indirect
)
