package main

// aws s3api list-buckets --endpoint-url=https://s3.cern.ch/\?usage --debug 2>&1 | grep \<Usage | sed "s/^b'//" | sed "s/'$//" | xmllint --format -

import (
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"time"

	"log/slog"

	sigv4 "github.com/imacks/aws-sigv4"
	"github.com/inhies/go-bytesize"
)

func main() {
	logger := slog.Default()

	accessKey := os.Getenv("ACCESS_KEY")
	if accessKey == "" {
		logger.Error("Required environment variable not set", "ACCESS_KEY")
		os.Exit(1)
	}

	secretKey := os.Getenv("SECRET_KEY")
	if secretKey == "" {
		logger.Error("Required environment variable not set", "SECRET_KEY")
		os.Exit(1)
	}

	s3Url := os.Getenv("S3_URL")
	if s3Url == "" {
		logger.Error("Required environment variable not set", "S3_URL")
		os.Exit(1)
	}

	exportBuckets := strings.Split(os.Getenv("EXPORT_BUCKETS"), ",")
	_ = exportBuckets

	usage, err := getUsage(s3Url, accessKey, secretKey)
	if err != nil {
		logger.Error("Failed to get bucket usage and quota", "error", err)
		os.Exit(1)
	}

	prettyPrint(usage)

}

func mustParseBytes(s string) bytesize.ByteSize {
	v, err := bytesize.Parse(s + "B")
	if err != nil {
		panic(err)
	}
	return v
}

func prettyPrint(usage UsageResponse) {
	fmt.Printf("Quota: %s, %s buckets, %s objects\n", mustParseBytes(usage.Summary.QuotaMaxBytes), usage.Summary.QuotaMaxBuckets, usage.Summary.QuotaMaxObjCount)


	for _, bucket := range 	usage.CapacityUsed.User.Buckets.Entry {
		fmt.Printf("%s: %s\n", bucket.Bucket, mustParseBytes(bucket.BytesRounded))
	}

	fmt.Printf("TOTAL: %s, %s objects\n", mustParseBytes(usage.Summary.TotalBytesRounded), usage.Summary.TotalEntries)
}

func getUsage(s3Url, accessKey, secretKey string) (UsageResponse, error){
	var usage UsageResponse

	// <S3_URL>/?usage is a Ceph S3 specific endpoint that returns information about quota and usage
	// https://docs.ceph.com/en/latest/radosgw/s3/serviceops/#get-usage-stats
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/?usage", s3Url), nil)
	if err != nil {
		return usage, fmt.Errorf("Unable to build request: %w", err)
	}
	req.Header.Add("X-Amz-Content-SHA256", sigv4.EmptyStringSHA256) // GET request has no content

	httpSigner, err := sigv4.New(
		sigv4.WithCredential(accessKey, secretKey, ""),
		sigv4.WithRegionService("us-east-1", "s3"), // region is not relevant for Ceph S3
	)
	if err != nil {
		return usage, fmt.Errorf("Failed to set up S3 HTTP signer with credentials: %w", err)
	}

	err = httpSigner.Sign(
		req, // the request that should be signed (headers will be modified in-place)
		sigv4.EmptyStringSHA256, // no body on the GET request, hence no payload hash
		sigv4.NewTime(time.Now()), // the signature should be valid from now on
	)
	if err != nil {
		return usage, fmt.Errorf("Unable to sign HTTP request: %w", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return usage, fmt.Errorf("Failed to do HTTP request: %w", err)
	}

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return usage, fmt.Errorf("Failed to read HTTP response body: %w", err)
	}

	if resp.StatusCode != 200 {
		return usage, fmt.Errorf("Server replied with unexpected %s: %s", resp.Status, bodyBytes)
	}

    if err := xml.Unmarshal(bodyBytes, &usage); err != nil {
		return usage, fmt.Errorf("Unable to decode HTTP response body: %w", err)
    }

	return usage, nil
}

type UsageResponse struct {
	XMLName xml.Name `xml:"Usage" json:"usage,omitempty"`
	Text    string   `xml:",chardata" json:"text,omitempty"`
	Entries string   `xml:"Entries"`
	Summary struct {
		Text                      string `xml:",chardata" json:"text,omitempty"`
		QuotaMaxBytes             string `xml:"QuotaMaxBytes"`
		QuotaMaxBuckets           string `xml:"QuotaMaxBuckets"`
		QuotaMaxObjCount          string `xml:"QuotaMaxObjCount"`
		QuotaMaxBytesPerBucket    string `xml:"QuotaMaxBytesPerBucket"`
		QuotaMaxObjCountPerBucket string `xml:"QuotaMaxObjCountPerBucket"`
		TotalBytes                string `xml:"TotalBytes"`
		TotalBytesRounded         string `xml:"TotalBytesRounded"`
		TotalEntries              string `xml:"TotalEntries"`
	} `xml:"Summary" json:"summary,omitempty"`
	CapacityUsed struct {
		Text string `xml:",chardata" json:"text,omitempty"`
		User struct {
			Text    string `xml:",chardata" json:"text,omitempty"`
			Buckets struct {
				Text  string `xml:",chardata" json:"text,omitempty"`
				Entry []struct {
					Text         string `xml:",chardata" json:"text,omitempty"`
					Bucket       string `xml:"Bucket"`
					Bytes        string `xml:"Bytes"`
					BytesRounded string `xml:"Bytes_Rounded"`
				} `xml:"Entry" json:"entry,omitempty"`
			} `xml:"Buckets" json:"buckets,omitempty"`
		} `xml:"User" json:"user,omitempty"`
	} `xml:"CapacityUsed" json:"capacityused,omitempty"`
}
